resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'


client_scripts {
	'spawn/client.lua',
	'vehicle-health-system/client.lua',
	'outlawalert/client.lua',
	'afkkick/client.lua',
	'anticheat/anticheat_client.lua',
	'anuncios/client.lua',
	'timesync/client/time.lua',
	'interiores/config.lua',
	'interiores/gui.lua',
	'interiores/client.lua',
	'driftonoff/drift.lua',
	'marcadagua/watermark.lua',
	'newlifeRP/server_name.lua',
	'npcsnewlife/client.lua',
	'SemPolicia/nowanted.lua',
	'streetLabel/config.lua',
	'streetLabel/client.lua',
	'dv/client.lua',
	'servicos/client.lua',
	'speedcamera/client.lua',
	'stungun/client.lua'
	
	
}
server_script {
	'outlawalert/server.lua',
	'afkkick/server.lua',
	'anticheat/anticheat_server.lua',
	'timesync/server/time.lua',
	'dv/server.lua'
}