--[[ ARMAS POLICIA MILITAR
police_weapons = {}
police_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
    ["WEAPON_NIGHTSTICK"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- ARMAS POLICIA MILITAR FORCA TATICA
local pmft_weapons = {}
pmft_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
    ["WEAPON_NIGHTSTICK"] = {ammo=200},
    ["WEAPON_SMG"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- ARMAS ROTA
local rota_weapons = {}
rota_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
    ["WEAPON_NIGHTSTICK"] = {ammo=200},
    ["WEAPON_SPECIALCARBINE"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- ARMAS POLICIA CIVIL
local pcivil_weapons = {}
pcivil_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_NIGHTSTICK"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- ARMAS POLICIA CIVIL - GARRA
garra_weapons = {}
garra_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
    ["WEAPON_SPECIALCARBINE"] = {ammo=200},
    ["WEAPON_NIGHTSTICK"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- ARMAS POLICIA CIVIL - GARRA
local prf_weapons = {}
prf_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
    ["WEAPON_SPECIALCARBINE"] = {ammo=200},
    ["WEAPON_NIGHTSTICK"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- ARMAS POLICIA ATIRADOR
sniper_weapons = {}
sniper_weapons["Equipar"] = {function(player,choice)
  vRPclient.giveWeapons(player,{
    ["WEAPON_COMBATPISTOL"] = {ammo=200},
    ["WEAPON_MARKSMANRIFLE"] = {ammo=200},
    ["WEAPON_SMOKEGRENADE"] = {ammo=5}
  }, true)
  BMclient.setArmour(player,100,true)
end}

-- BOMBEIROS
local bombeiros_kit = {}
bombeiros_kit["Equipar"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  vRP.giveInventoryItem(user_id,"medkit",25,true)
  vRPclient.giveWeapons(player,{
    ["WEAPON_FIREEXTINGUISHER"] = {ammo=200},
    ["WEAPON_HATCHET"] = {ammo=200},
    ["WEAPON_STUNGUN"] = {ammo=200}
  }, true)
  --vRP.giveInventoryItem(user_id,"pills",25,true)
end}

--medkit storage
local emergency_medkit = {}
emergency_medkit["Equipar"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  vRP.giveInventoryItem(user_id,"medkit",25,true)
  --vRP.giveInventoryItem(user_id,"pills",25,true)
end}

--heal me
local emergency_heal = {}
emergency_heal["Equipar"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  vRPclient.setHealth(player,1000)
end}

]]