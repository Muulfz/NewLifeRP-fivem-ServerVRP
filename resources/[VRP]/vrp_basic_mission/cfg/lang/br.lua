
local lang = {
  repair = "Reparar {1}.",
  transfer = "Transfira o paciente.",
  reward = "Recompensa: R${1}.",
  delivery = {
    title = "Entrega",
    item = "- {2} {1}"
  },
  carjack =  "Roube e entregue um veículo.",
  shipment =  "Receba a mercadoria.",
  weaponrec =  "Recupere suas coisas.",
  recsucces = "Você recuperou suas armas.",
  cooldown =  "Cooldown: {1} segundo.",
  own_veh = "Você não pode entregar seu próprio veículo.",
  in_veh = "Por favor, saia do veículo.",
  no_veh = "Você não está em um veículo."
}

return lang
