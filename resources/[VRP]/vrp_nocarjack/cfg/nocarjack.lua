cfg = {}

cfg.chance = 100 -- chance of being unlocked in percentage

cfg.blacklist = { -- vehicles that will always be locked when spawned naturally
  "T20",
  "OSIRIS",
  "INFERNUS",
  "ADDER",
  "ENTITYXF",
  "TURISMOR",
  "CHEETAH",
  "POLICE",
  "POLICE3",
  "POLICE4",
  "POLICE2",
  "POLICET",
  "RIOT",
  "SHERIFF",
  "SHERIFF2",
  "POLICEB",
  "FBI",
  "FBI2",
  "CARGOBOB4",
  "SUPERVOLITO",
  "SUPERVOLITO2",
  "VALKYRIE2",
  "VOLATUS",
  "SWIFT",
  "SAVAGE",
  "VALKYRIE",
  "ANNIHILATOR",
  "BUZZARD",
  "BUZZARD2",
  "CARGOBOB",
  "CARGOBOB2",
  "CARGOBOB3",
  "SKYLIFT",
  "POLMAV",
  "MAVERICK",
  "FROGGER",
  "FROGGER2",
  "SWIFT2",
  "PREDATOR",
  "RHINO"
}

return cfg