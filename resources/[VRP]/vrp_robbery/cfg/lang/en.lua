local lang = {
  blip = "Roubo",
  cops = {
    cant_rob = "~r~Os policias não podem roubar!",
	not_enough = "~r~Você precisa de {3} policiais onlines"
  },
  robbery = {
	wait = "Isso foi roubado recentemente. Espere um pouco: ^2{1}^0 segundos.",
	progress = "Roubo está em progresso ^2{1}",
	started = "Você começou o roubo às: ^2{1}^0, não vá muito longe desse ponto!",
	hold = "Segure por pelo menos ^1{1} ^0minutos e o dinheiro será seu!",
	over = "Roubo terminou às: ^2{1}^0!",
	canceled = "Roubo foi cancelado às: ^2{1}^0!",
	done = "Roubo terminado, você recebeu: ^2{1}^0!"
  },
  title = {
    robbery = "ROUBO",
	news = "NOTICIAS",
	system = "SISTEMAS"
  },
  client = {
	rob = "Pressione ~INPUT_RELOAD~ para roubar ~b~{1}~w~ cuidado a policia vai ser alertada!",
    robbing = "Roubando: ~r~{1}~w~ segundos restantes",
	canceled = "O roubo foi cancelado, você não receberá nada."
  }
}

return lang