
local cfg = {}

-- define static item transformers
-- see https://github.com/ImagicTheCat/vRP to understand the item transformer concept/definition

cfg.item_transformers = {
  -- example of harvest item transformer
 --[[  {
    name="Water bottles/tacos tree", -- menu name
    -- permissions = {"harvest.water_bottle_tacos"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=20,
    units_per_minute=10,
    x=1861,y=3680.5,z=33.26, -- pos
    radius=5, height=1.5, -- area
    recipes = {
      ["Harvest water"] = { -- action name
        description="Harvest some water bottles.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["water"] = 1
        }
      },
      ["Harvest tacos"] = { -- action name
        description="Harvest some tacos.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["tacos"] = 1
        }
      }
    }
    --, onstart = function(player,recipe) end, -- optional start callback
    -- onstep = function(player,recipe) end, -- optional step callback
    -- onstop = function(player,recipe) end -- optional stop callback
  }, ]]
  
  --ACADEMIA
  {
    name="Academia", -- menu name
    r=255,g=125,b=0, -- color
    max_units=1000,
    units_per_minute=1000,
    x=-1202.96252441406,y=-1566.14086914063,z=4.61040639877319,
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Força"] = { -- action name
        description="Aumente sua força e stamina", -- action description
        in_money=1, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={ -- optional
          ["physical.strength"] = 4 -- "group.aptitude", give 4 exp per unit
        }
      }
    }
  }
}

-- define transformers randomly placed on the map
cfg.hidden_transformers = {
  ---------------------------DROGAS -----------------------

  --                         CAMPOS                      --
  --CAMPO DE MACONHA
  {
    name="Campo de Ervas", -- menu name
    permissions = {"colher.maconha"}, -- you can add permissions
    r=0,g=200,b=0, -- color
    max_units=200,
    units_per_minute=5,
    x=-292.09762573242,y=1389.9826660156,z=345.90356445313, -- pos
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- action name
        description="Colher maconha.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["weed"] = 1
        }
      }
    }
  },
  -- CAMPO DE METANFETAMINA    
  {
    name="Campo de Metanfetamina", -- Nome do menu
    permissions = {"colher.meta"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=1920.3999023438,y=417.48077392578,z=161.60873413086, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Crystal Melamine.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["crystalmelamine"] = 1
        }
      }
    }
  },  
  -- CAMPO DE HEROINA 
  {
    name="Campo de Heroina", -- Nome do menu
    permissions = {"campo.heroina"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=1614.4848632813,y=-2634.6943359375,z=42.580905914307, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Papaver somniferum.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["opio"] = 1
        }
      }
    }
  },   
  -- CAMPO DE CRACK  
  {
    name="Campo de Crack", -- Nome do menu
    permissions = {"campo.crack"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=-2977.6457519531,y=1607.5068359375,z=21.268037796021, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Crystal de Crack.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["cristal"] = 1
        }
      }
    }
  },  
  -- CAMPO DE LANCA 
  {
    name="Campo de Lanca perfume", -- Nome do menu
    permissions = {"campo.lanca"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=2821.0607910156,y=-741.66400146484,z=1.9846405982971, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher Cloreto de etila.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["cloreto"] = 1
        }
      }
    }
  },   
  -- CAMPO DE COCAINA    
  {
    name="Campo de Cocaina", -- Nome do menu
    permissions = {"campo.cocaina"}, -- Você pode adicionar permissões
    r=0,g=200,b=0, -- cor
    max_units=200,
    units_per_minute=5,
    x=-1764.9029541016,y=-89.266693115234,z=81.877517700195, -- Localização
    radius=7.5, height=1.5, -- area
    recipes = {
      ["Colher"] = { -- Nome da ação
        description="Colher folha de Coca.", -- Descrição do produto a se colher
        in_money=0, -- Dinheiro dado por unidade
        out_money=0, -- Dinheiro ganho por unidade
        reagents={}, -- Itens colhidos por unidade
        products={ -- Nome do produto fornecido por unidade
          ["folhadecoca"] = 1
        }
      }
    }
  },
  --                     PROCESSAMENTO                  --

  -- PROCESSADOR DE METANFETAMINA
  {
    name="Processador de Metanfetamina", -- menu name
    permissions = {"process.meta"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=845.31506347656,y=-291.71832275391,z=73.256881713867, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Metanfetamina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["crystalmelamine"] = 1
      },
        products={ -- items given per unit
          ["metanfetamina"] = 2
        }
      }
    }
  }, 
  -- PROCESSAMENTO DE HEROINA  
  {
    name="Processador de Heroina", -- menu name
    permissions = {"process.heroina"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1320.4366455078,y=-473.33395385742,z=81.444160461426, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Heroina.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["opio"] = 1
      },
        products={ -- items given per unit
          ["heroina"] = 2
        }
      }
    }
  },
  -- PROCESSADOR DE COCAINA
  {
    name="Processador de cocaina", -- menu name
    permissions = {"process.cocaina"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=997.75207519531,y=-128.54899597168,z=86.507797241211, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Cocaina.", -- action description
        in_money=4, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["folhadecoca"] = 1
      },
        products={ -- items given per unit
          ["cocaina"] = 1
        }
      }
    }
  },
  -- PROCESSADOR DE COCAINA LOBOS
  {
    name="Processador de cocaina2", -- menu name
    permissions = {"process.cocaina2"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1981.7066650391,y=3030.4665527344,z=47.056289672852, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Cocaina.", -- action description
        in_money=4, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["folhadecoca"] = 1
      },
        products={ -- items given per unit
          ["cocaina"] = 1
        }
      }
    }
  },
    -- PROCESSAMENTO DE CRACK   
  {
    name="Processador de Crack", -- menu name
    permissions = {"process.crack"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1394.6291503906,y=3601.8801269531,z=38.94189453125, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Crack.", -- action description
        in_money=1, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["cristal"] = 1
      },
        products={ -- items given per unit
          ["crack"] = 1
        }
      }
    }
  },
  -- PROCESSADOR DE LANCA
  {
    name="Processador de Lanca Perfume", -- menu name
    permissions = {"process.lanca"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=1392.0981445313,y=3605.3110351563,z=38.941932678223,  -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Lanca Perfume.", -- action description
        in_money=1, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["cloreto"] = 1
      },
        products={ -- items given per unit
          ["lanca"] = 1
        }
      }
    }
  },
  -- PROCESSAMENTO DE MACONHA  
  {
    name="Processar Ervas", -- menu name
    permissions = {"process.weed"}, -- you can add permissions
    r=0,g=200,b=0, -- cor do menu
    max_units=200, -- unidades maximas do item
    units_per_minute=200, -- unidades que o transformador ganha de volta por minuto
    x=-810.89904785156,y=611.54016113281,z=131.46493530273, -- pos
    radius=5.5, height=1.5, -- area
    recipes = { -- items do menu
      ["Processar"] = { -- action name
        description="Processador de Maconha.", -- action description
        in_money=1, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={  -- items taken per unit
          ["weed"] = 1
    },
        products={ -- items given per unit
      ["cannabis"] = 1
        }
      }
    }
  },
  ---------------------OUTROS TRABALHOS--------------------

  --                       COLETA                        -- 
  -- CARRO FORTE
  {
    name="Cofre", -- menu name
    permissions = {"bankdriver.money"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=1,
    units_per_minute=1,
    x=253.90089416504,y=225.21408081055,z=101.87578582764,
    radius=2, height=1.0, -- area
    recipes = {
      ["Dinheiro do Banco"] = { -- action name
        description="Pegar dinheiro do banco.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
          ["bank_money"] = 500000
        }, -- items given per unit
        aptitudes={} -- optional
      }
     }
  },


}

-- time in minutes before hidden transformers are relocated (min is 5 minutes)
cfg.hidden_transformer_duration = 5*24*60 -- 5 days

-- configure the information reseller (can sell hidden transformers positions)
cfg.informer = {
  infos = {
    ["weed field"] = 20000
  },
  positions = {
    {1821.12390136719,3685.9736328125,34.2769317626953},
    {1804.2958984375,3684.12280273438,34.217945098877}
  },
  interval = 60, -- interval in minutes for the reseller respawn
  duration = 10, -- duration in minutes of the spawned reseller
  blipid = 133,
  blipcolor = 2
}

return cfg
