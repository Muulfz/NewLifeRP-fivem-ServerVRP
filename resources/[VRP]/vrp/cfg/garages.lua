
local cfg = {}
-- define garage types with their associated vehicles
-- (vehicle list: https://wiki.fivem.net/wiki/Vehicles)

-- each garage type is an associated list of veh_name/veh_definition 
-- they need a _config property to define the blip and the vehicle type for the garage (each vtype allow one vehicle to be spawned at a time, the default vtype is "default")
-- this is used to let the player spawn a boat AND a car at the same time for example, and only despawn it in the correct garage
-- _config: vtype, blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)

cfg.rent_factor = 0.1 -- 10% of the original price if a rent
cfg.sell_factor = 0.75 -- sell for 75% of the original price

cfg.garage_types = { 
 --[[ ["Barco de Pesca"] = {
    _config = {vtype="car",blipid=427,blipcolor=28,permissions="garagem.pescador"},
    ["suntrap"] = {"Barco de Pesca",0, "O melhor basco de pesca!"}
  },

  ["Transporte de Valores"] = {
    _config = {vtype="car",blipid=67,blipcolor=4,permissions="garagem.carroforte"},
    ["stockade"] = {"Carro Forte",0, ""}
  },

  ["Caminhoneiro"] = {
    _config = {vtype="car",blipid=67,blipcolor=4,permissions="garagem.caminheiro"},
    ["daf"] = {"Daf XF 105",381000, ""}
  },

  ["Policia Rodoviaria Federal"] = {
    _config = {vtype="car",permissions="garagem.prf"},
    ["blazerprf"] = {"Blazer PRF",0, ""},
    ["dakarprf"] = {"Dakar PRF",0, ""},
    ["nissanprf"] = {"Nissan PRF",0, ""}
  },

  ["Policia Militar"] = {
    _config = {vtype="car",permissions={"garagem.pm"}},
    ["pmcobalt"] = {"Cobalt PM",0, ""},
    ["pmfocus"] = {"Focus PM",0, ""},
    ["pmpalio"] = {"Palio PM",0, ""},
    ["pmpassat"] = {"Passat PM - Em teste",0, ""},
    ["pmvan"] = {"Van PM - Em teste",0, ""},
    ["ftsw4"] = {"SW4 Força Tatica - Testes",0, ""}
  },

  ["Força Tatica Policia Militar"] = {
    _config = {vtype="car",permissions="garagem.pmft"},
    ["trailft"] = {"Trail Blazer Força Tatica",0, ""},
    ["blazerft"] = {"Blazer Força Tatica",0, ""}
  },

  ["ROCAM Policia Militar"] = {
    _config = {vtype="car",permissions="garagem.rocam"},
     ["tigerrocam"] = {"Tigger Rocam",0, ""},
     ["pmcobalt"] = {"Cobalt PM",0, ""},
     ["pmpalio"] = {"Palio PM",0, ""}
  },

  ["Aguia Policia Militar"] = {
    _config = {vtype="car",permissions="garagem.aguia"},
     ["as350"] = {"Aguia",0, ""},
     ["pmcobalt"] = {"Cobalt PM",0, ""},
     ["pmpalio"] = {"Palio PM",0, ""}
  },

  ["Coronel ROTA"] = {
    _config = {vtype="car",permissions="garagem.rotacoronel"},
    ["rotatahoe"] = {"Rota Tahoe",0, ""}
  },

  ["ROTA"] = {
    _config = {vtype="car",permissions="garagem.rota"},
    ["rotatahoe"] = {"Rota Tahoe",0, ""},
    ["rotasw4"] = {"SW4 Rota - Testes",0, ""}
  },

 
  ["Helicóptero ROTA"] = {
    _config = {vtype="car",permissions="garagem.rotaheli"},
    ["valkyrie2"] = {"Helicoptero da Rota",0, ""},
  },

  ["Policia Civil"] = {
    _config = {vtype="car",permissions="garagem.pc"},
    ["blazerpc"] = {"Blazer Policia Civil",0, ""},
    ["trailpc"] = {"Trail Blazer Policia Civil",0, ""},
    ["descpc"] = {"Descaracterizada Policia Civil",0, ""},
    ["garrasw4"] = {"Garra Policia Civil",0, ""}
  },

  ["Pelicano Policia Civil"] = {
    _config = {vtype="car",permissions="garagem.pcheli"},
    ["pcheli"] = {"Pelicano",0, ""}
  },

  ["SAMU"] = {
    _config = {vtype="car",permissions="garagem.hospital"},
    ["mastersamu"] = {"Ambulancia",0, ""}
  },

  ["Bombeiros"] = {
    _config = {vtype="car",permissions="garagem.bombeiros"},
    ["mastersamu"] = {"Ambulancia",0, ""}
  },

  ["Garagem Globo News"] = {
    _config = {vtype="car",permissions="garagem.globonews"},
    ["speedo3"] = {"Van da Globo",0, ""}
  },

  ["uber"] = {
    _config = {vtype="car",blipid=198,blipcolor=5,permissions="garagem.uber"},
    ["fusiont"] = {"Uber",0, ""}
  },

  ["Mecanico"] = {
    _config = {vtype="car",blipid=85,blipcolor=31,permissions="garagem.mecanico"},
    ["towtruck2"] = {"towtruck2",0, ""}
  },
  
  ["Entregador"] = {
    _config = {vtype="car",blipid=355,blipcolor=4,permissions="garagem.entregador"},
    ["enduro"] = {"Fan 150",8000, ""}
  },--]]
}

-- {garage_type,x,y,z}
cfg.garages = { --[[
  
  --POLICIA MILITAR  
  {"Coronel Policia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"Coronel Policia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"Coronel Policia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"Coronel Policia Militar",901.92004394531,-8.369234085083,78.764854431152},
  {"Coronel Policia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"Policia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"Policia Militar",229.58934020996,-367.59896850586,44.163722991943},
  {"Policia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"Policia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"Policia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"ROCAM Policia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"ROCAM Policia Militar",229.58934020996,-367.59896850586,44.163722991943},
  {"ROCAM Policia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"ROCAM Policia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"ROCAM Policia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"Força Tatica Policia Militar",453.24819946289,-1019.3926391602,28.396326065063},
  {"Força Tatica Policia Militar",229.58934020996,-367.59896850586,44.163722991943},
  {"Força Tatica Policia Militar",1871.9401855469,3688.9060058594,33.641696929932},
  {"Força Tatica Policia Militar",827.94219970703,-1258.1525878906,26.283124923706},
  {"Força Tatica Policia Militar",374.07794189453,-1620.5498046875,29.291946411133},
  {"Aguia Policia Militar",450.23489379883,-981.12622070313,43.691707611084},
  {"Aguia Policia Militar",362.91030883789,-1598.5764160156,36.948837280273},
  {"Aguia Policia Militar",-1095.6641845703,-834.990234375,37.675407409668},
  
  
  -- PM ROTA  
  {"Coronel ROTA",453.24819946289,-1019.3926391602,28.396326065063},
  {"Coronel ROTA",1871.9401855469,3688.9060058594,33.641696929932},
  {"Coronel ROTA",827.94219970703,-1258.1525878906,26.283124923706},
  {"Coronel ROTA",901.92004394531,-8.369234085083,78.764854431152},
  {"Coronel ROTA",374.07794189453,-1620.5498046875,29.291946411133},
  {"ROTA",453.24819946289,-1019.3926391602,28.396326065063},
  {"ROTA",229.58934020996,-367.59896850586,44.163722991943},
  {"ROTA",1871.9401855469,3688.9060058594,33.641696929932},
  {"ROTA",827.94219970703,-1258.1525878906,26.283124923706},
  {"ROTA",901.92004394531,-8.369234085083,78.764854431152},
  {"Helicóptero ROTA",885.0830078125,-40.144481658936,79.643371582031},
  
  
  --POlicia Rodoviaria Federal
  {"Policia RodoviAria Federal",-461.98156738281,6019.5883789063,30.6907081604},
  {"Policia RodoviAria Federal",-475.07077026367,5988.4794921875,31.33670425415},
  
  
  --Policia Civil
  
  {"Policia Civil",453.24819946289,-1019.3926391602,28.396326065063},
  {"Policia Civil",229.58934020996,-367.59896850586,44.163722991943},
  {"Policia Civil",1871.9401855469,3688.9060058594,33.641696929932},
  {"Policia Civil",827.94219970703,-1258.1525878906,26.283124923706},
  {"Policia Civil",374.07794189453,-1620.5498046875,29.291946411133},
  {"Pelicano Policia Civil",450.23489379883,-981.12622070313,43.691707611084},
  {"Pelicano Policia Civil",362.91030883789,-1598.5764160156,36.948837280273},
  {"Pelicano Policia Civil",-1095.6641845703,-834.990234375,37.675407409668},
  
  -- Helicoptero Rota
  {"Helicóptero ROTA",885.0830078125,-40.144481658936,79.643371582031},
--  {"compacts",-356.146, -134.69, 39.0097},
--  {"coupe",723.013, -1088.92, 22.1829}
--  {"sports",-1145.67, -1991.17, 13.162},
--  {"sportsclassics",1174.76, 2645.46, 37.7545},
--  {"supercars",112.275, 6619.83, 31.8154},
--  {"motorcycles",-205.789, -1308.02, 31.2916}
  --{"planes",1640, 3236, 40.4},
  --{"planes",2123, 4805, 41.19},
  --{"planes",-1348, -2230, 13.9},
  --{"helicopters",1750, 3260, 41.37},
  --{"helicopters",-1233, -2269, 13.9},
  --{"helicopters",-745, -1468, 5},
  --{"boats",-849.5, -1368.64, 1.6},
  --{"boats",1538, 3902, 30.35} --]]
}

return cfg
