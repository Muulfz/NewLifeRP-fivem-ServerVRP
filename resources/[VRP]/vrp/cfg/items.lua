-- define items, see the Inventory API on github

local cfg = {}

-- see the manual to understand how to create parametric items
-- idname = {name or genfunc, description or genfunc, genfunc choices or nil, weight or genfunc}
-- a good practice is to create your own item pack file instead of adding items here
cfg.items = {
-- Droga
  ["cannabis"] = {"Maconha", "Processada.", nil, 1.00}, -- no choices
  ["cocaina"] = {"Cocaina", "Cocaina em po.", nil, 1.00}, -- no choices
  ["metanfetamina"] = {"Metanfetamina", "Cristal de Metanfetamina.", nil, 1.00}, -- no choices  
  ["heroina"] = {"Heroina", "Diacetilmorfina.", nil, 1.00}, -- no choices  
  ["crack"] = {"Crack", "Cristais de cocaina.", nil, 1.00}, -- no choices  
  ["lanca"] = {"Lanca Perfume", "Cloreto de etila.", nil, 1.00}, -- no choices  
--Fonte
  ["folhadecoca"] = {"Folha de Coca", "Folha de Coca.", nil, 1.00}, -- no choices
  ["crystalmelamine"] = {"Crystal Melamine", "Crystal Melamine.", nil, 1.00}, -- no choices
  ["weed"] = {"Planta de maconha", "Planta de maconha.", nil, 1.00}, -- no choices
  ["opio"] = {"Opiode", "Papaver somniferum.", nil, 1.00}, -- no choices
  ["cristal"] = {"Cristal de Cocaina", "Cristal de Crack", nil, 1.00}, -- no choices
  ["cloreto"] = {"Cloreto de etila", "cloreto de etila.", nil, 1.00}, -- no choices
  ["clonazepam"] = {"Clonazepam", "Clonazepam.", nil, 1.00}, -- no choices
  ["paracetamol"] = {"Paracetamol", "Paracetamol.", nil, 1.00}, -- no choices
-- outros
  ["Pedra"] = {"Pedra", "Pedra bruta.", nil, 0.01}, -- no choices
  ["Minerio"] = {"Minerio", "Minerio refinado.", nil, 0.01}, -- no choices
  ["portedearma"] = {"Porte de Arma", "licença para arma.", nil, 0.01}, -- no choices
  ["oab"] = {"OAB", "Carteira de Advogado.", nil, 0.01}, -- no choices
  ["bank_money"] = {"Dinheiro do Banco", "R$.", nil, 0}, -- no choices
  ["credit"] = {"Cartao de Credito", "Credit card.", nil, 0.01}, -- no choices
  ["bank_money"] = {"Dinheiro do Banco", "R$.", nil, 0}, -- no choices
  ["dinheiro_roubo"] = {"Dinheiro Marcado", "R$.", nil, 0} -- no choices
}

-- load more items function
local function load_item_pack(name)
  local items = module("cfg/item/"..name)
  if items then
    for k,v in pairs(items) do
      cfg.items[k] = v
    end
  else
    print("[Brasil New life RolePlay] Pacote de Itens ["..name.."] não encontrado")
  end
end

-- PACKS
load_item_pack("required")
load_item_pack("food")
load_item_pack("drugs")

return cfg
