
-- this file configure the cloakrooms on the map

local cfg = {}

-- prepare surgeries customizations
local surgery_male = { model = "mp_m_freemode_01" }
local surgery_female = { model = "mp_f_freemode_01" }
local uniforme_pm = { model = "s_m_y_cop_01" }
local uniforme_pmf = { model = "s_f_y_sheriff_01" }
local uniforme_capitao = { model = "s_m_y_cop_01" }
local uniforme_aguia = { model = "s_m_y_cop_01" }
local uniforme_rota = { model = "s_m_y_sheriff_01" }
local uniforme_rocam = { model = "s_m_y_hwaycop_01" }
local uniforme_bope = { model = "s_m_y_swat_01" }
local uniforme_tatica = { model = "s_m_m_snowcop_01" }
local uniforme_samu = { model = "s_m_m_paramedic_01" }
local uniforme_civil = { model = "s_m_m_ciasec_01" }
local uniforme_delegado = { model = "s_f_y_sheriff_01" }
local uniforme_civil2 = { model = "s_m_m_fibsec_01" }
local uniforme_garra = { model = "s_m_y_blackops_03" }
local uniforme_bombeiro1 = { model = "s_m_y_fireman_01" }
local uniforme_bombeiro2 = { model = "s_m_y_pestcont_01" }
local uniforme_bombeiro3 = { model = "cs_brad" }
local uniforme_prf = { model = "s_m_y_hwaycop_01" }

for i=0,19 do
  uniforme_pm[i] = {0,0}
  uniforme_pmf[i] = {0,0}
  uniforme_capitao[i] = {0,0}
  uniforme_aguia[i] = {0,0}
  uniforme_samu[i] = {0,0}
  uniforme_rocam[i] = {0,0}
  uniforme_bope[i] = {0,0}
  uniforme_tatica[i] = {0,0}
  uniforme_rota[i] = {0,0}
  surgery_female[i] = {0,0}
  surgery_male[i] = {0,0}
  uniforme_civil[i] = {0,0}
  uniforme_civil2[i] = {0,0}
  uniforme_delegado[i] = {0,0}
  uniforme_garra[i] = {0,0}
  uniforme_bombeiro1[i] = {0,0}
  uniforme_bombeiro2[i] = {0,0}
  uniforme_bombeiro3[i] = {0,0}
  uniforme_prf[i] = {0,0}
end

-- cloakroom types (_config, map of name => customization)
--- _config:
---- permissions (optional)
---- not_uniform (optional): if true, the cloakroom will take effect directly on the player, not as a uniform you can remove
cfg.cloakroom_types = {
  ["Polícia Militar"] = {
    _config = { permissions = {"pm.cloakroom"} },
    ["Uniforme Polícia Militar"] = uniforme_pm,
    ["Uniforme feminino Polícia Militar"] = uniforme_pmf,
  },
  ["ROTA"] = {
    _config = { permissions = {"rota.cloakroom"} },
    ["Uniforme ROTA"] = uniforme_rota,
  },
  ["Vestuário do Coronel"] = {
    _config = { permissions = {"coronel.cloakroom"} },
    ["Uniforme ROTA"] = uniforme_rota,
    ["Uniforme ROCAM"] = uniforme_rocam,
    ["Uniforme Polícia Militar"] = uniforme_pm,
    ["Uniforme feminino Polícia Militar"] = uniforme_pmf,
    ["Uniforme Águia"] = uniforme_aguia,
    ["Uniforme Força Tática"] = uniforme_tatica,
  },
  ["Força Tática"] = {
    _config = { permissions = {"tatica.cloakroom"} },
    ["Uniforme Força Tática"] = uniforme_tatica,
    ["Uniforme Polícia Militar"] = uniforme_pm,
  },
  ["ROCAM"] = {
    _config = { permissions = {"rocam.cloakroom"} },
    ["Uniforme ROCAM"] = uniforme_rocam,
    ["Uniforme Polícia Militar"] = uniforme_pm,
  },
  ["Águia"] = {
    _config = { permissions = {"aguia.cloakroom"} },
    ["Uniforme Águia"] = uniforme_bope,
    ["Uniforme Polícia Militar"] = uniforme_pm,
  },
  ["Polícia Civil"] = {
    _config = { permissions = {"civil.cloakroom"} },
    ["Uniforme Polícia Civil"] = uniforme_civil,
    ["Uniforme GARRA"] = uniforme_garra,
    ["Uniforme Investigativo"] = uniforme_civil2,
  },
  ["GARRA - Polícia Civil"] = {
    _config = { permissions = {"garra.cloakroom"} },
    ["Uniforme GARRA"] = uniforme_garra,
    ["Uniforme Investigativo"] = uniforme_civil2,
  },
  ["Bombeiros"] = {
    _config = { permissions = {"bombeiros.cloakroom"} },
    ["Uniforme Bombeiro Inicial"] = uniforme_bombeiro1,
    ["Uniforme Bombeiro Secundario"] = uniforme_bombeiro2,
    ["Uniforme Bombeiro Ultimated"] = uniforme_bombeiro3,
  },
  ["Polícia Rodoviária Federal"] = {
    _config = {permissions = {"prf.cloakroom"}},
    ["Uniforme PRF"] = uniforme_prf,
  },
  ["Emergência"] = {
    _config = { permissions = {"samu.cloakroom"} },
    --[[["Male uniform"] = {
      [3] = {92,0},
      [4] = {9,3},
      [6] = {25,0},
      [8] = {15,0},
      [11] = {13,3},
      ["p2"] = {2,0}
    }]]
    ["Uniforme SAMU"] = uniforme_samu,
  },
  ["Escolher Sexo"] = {
    _config = { not_uniform = true },
    ["Male"] = surgery_male,
    ["Female"] = surgery_female
  }
}


cfg.cloakrooms = {
  --{"police",399.23114013672,-1613.9088134766,19.167358398438},
  {"Polícia Militar",399.23114013672,-1613.9088134766,19.167358398438},
  {"Vestuário do Coronel",399.23114013672,-1613.9088134766,19.167358398438},
  {"Força Tática",399.23114013672,-1613.9088134766,19.167358398438},
  {"Águia",399.23114013672,-1613.9088134766,19.167358398438},
  {"ROTA",457.53500366211,-991.83148193359,30.689584732056},
  {"ROTA",346.53405761719,-1613.7239990234,19.145702362061},
  {"ROCAM",457.53500366211,-991.83148193359,30.689584732056},
  {"ROCAM",399.23114013672,-1613.9088134766,19.167358398438},
  {"GARRA - Polícia Civil",457.53500366211,-991.83148193359,30.689584732056},
  {"Polícia Civil",457.53500366211,-991.83148193359,30.689584732056},
  {"Escolher sexo",230.81727600098,-405.87521362305,47.924365997314},
  {"Bombeiros",2061.3322753906,-461.58746337891,14.7248687744140},
  {"Emergência",269.77987670898,-1363.4407958984,24.537780761719},
  {"Escolher Sexo",230.81727600098,-405.87521362305,47.924365997314},
  {"Polícia Rodoviária Federal",-449.84616088867,6016.4267578125,31.71639251709}
}


return cfg
