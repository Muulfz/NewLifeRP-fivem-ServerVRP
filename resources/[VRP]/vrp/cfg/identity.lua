
local cfg = {}

-- city hall position
cfg.city_hall = {236.872360229492, -408.259490966797, 47.9243659973145}

-- cityhall blip {blipid,blipcolor}
cfg.blip = {498,4}

-- cost of a new identity
cfg.new_identity_cost = 500000

-- phone format (max: 20 chars, use D for a random digit)
cfg.phone_format = "(011)9DDDD-DDDD"
-- cfg.phone_format = "06DDDDDDDD" -- another example for cellphone in France


-- config the random name generation (first join identity)
-- (I know, it's a lot of names for a little feature)
-- (cf: http://names.mongabay.com/most_common_surnames.htm)
cfg.random_first_names = {
  "Sem"
}

cfg.random_last_names = {
  "Indentidade"
}

return cfg
