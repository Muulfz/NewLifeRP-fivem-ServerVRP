
-- this file configure the cloakrooms on the map

local cfg = {}

-- weaponkits types (_config, map of name => customization)
--- _config:
---- permissions (optional)
cfg.weapons_kits_types = {
  ["pm_weapons"] = {
    _config = {
	  permissions = {"armas.pm"},
	  title = {"Policia Militar"}
	},
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
        ["WEAPON_NIGHTSTICK"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
  },
  ["emergency_heal"] = {
    _config = {
	  permissions = {"civil.curar"},
	  title = {"Hospital"}
	},
    ["Curar"] = {}
  },
   ["pmft_weapons"] = {
    _config = {
      permissions = {"armas.pmft"},
      title = {"Força Tática - Polícia Militar"}
     },     
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
        ["WEAPON_NIGHTSTICK"] = {ammo=200},
        ["WEAPON_SMG"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
   },
   ["rota_weapons"] = {
    _config = {
      permissions = {"armas.rota"},
      title = {"ROTA - Polícia"}
     },     
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
        ["WEAPON_NIGHTSTICK"] = {ammo=200},
        ["WEAPON_SPECIALCARBINE"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
   },
   ["pcivil_weapons"] = {
    _config = {
     permissions = {"armas.pcbase"},
      title = {"Policia Civil"}
     },     
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_NIGHTSTICK"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
   },
   ["garra_weapons"] = {
    _config = {
      permissions = {"armas.pcgarra"},
      title = {"GARRA - Polícia Civil"}
     },     
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
        ["WEAPON_NIGHTSTICK"] = {ammo=200},
        ["WEAPON_SPECIALCARBINE"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
   },
   ["prf_weapons"] = {
    _config = {
      permissions = {"armas.prf"},
      title = {"Armas - Polícia Rodoviária Federal"}
     },     
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_PUMPSHOTGUN"] = {ammo=200},
        ["WEAPON_NIGHTSTICK"] = {ammo=200},
        ["WEAPON_SPECIALCARBINE"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
   },
   ["sniper_weapons"] = {
    _config = {
      permissions = {"armas.sniper"},
      title = {"Armas - Atirador de elite"}
     },     
    ["Equipar"] = {
        ["WEAPON_COMBATPISTOL"] = {ammo=200},
        ["WEAPON_MARKSMANRIFLE"] = {ammo=200},
        ["WEAPON_SMOKEGRENADE"] = {ammo=5}
    }
   },
   ["bombeiros_kit"] = {
    _config = {
      permissions = {"kit.bombeiros"},
      title = {"Kit - Bombeiros"}
     },     
    ["Equipar"] = {
        ["WEAPON_FIREEXTINGUISHER"] = {ammo=200},
        ["WEAPON_HATCHET"] = {ammo=200},
        ["WEAPON_STUNGUN"] = {ammo=200}
    }
   },
   ["emergency_medkit"] = {
    _config = {
      permissions = {"emergency.medkit"},
      title = {"Kit - Emergência"}
     },     
    ["Equipar"] = {
        ["medkit"] = {ammo=25}
    }
   }
}


cfg.weapons_kits = {  
  {"pm_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"pm_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"prf_weapons",-448.58901977539,6018.013671875,31.71639251709},
  {"pmft_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"pmft_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"pmft_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"rota_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"rota_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"rota_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"pcivil_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"pcivil_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"pcivil_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"garra_weapons",461.31414794922,-981.15582275391,30.689588546753},
  {"garra_weapons",397.48709106445,-1610.3175048828,19.167369842529},
  {"garra_weapons",351.48681640625,-1611.1232910156,19.145702362061},
  {"emergency_medkit",268.22784423828,-1364.8872070313,24.537782669067},
  {"bombeiros_kit",268.22784423828,-1364.8872070313,24.537782669067},
  {"emergency_heal",260.49597167969,-1358.4555664063,24.537788391113}
}


return cfg
