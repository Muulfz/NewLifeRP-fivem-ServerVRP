
-- cloakroom system
local lang = vRP.lang
local cfg = module("cfg/generalmenus")

-- build cloakroom menus

local menus = {}

-- -- save idle custom (return current idle custom copy table)
-- local function save_idle_custom(player, custom)
  -- local r_idle = {}

  -- local user_id = vRP.getUserId(player)
  -- if user_id then
    -- local data = vRP.getUserDataTable(user_id)
    -- if data then
      -- if data.cloakroom_idle == nil then -- set cloakroom idle if not already set
        -- data.cloakroom_idle = custom
      -- end

      -- -- copy custom
      -- for k,v in pairs(data.cloakroom_idle) do
        -- r_idle[k] = v
      -- end
    -- end
  -- end

  -- return r_idle
-- end

-- local function rollback_idle_custom(player)
  -- local user_id = vRP.getUserId(player)
  -- if user_id then
    -- local data = vRP.getUserDataTable(user_id)
    -- if data then
      -- if data.cloakroom_idle ~= nil then -- consume cloakroom idle
        -- vRPclient.setCustomization(player,data.cloakroom_idle)
        -- data.cloakroom_idle = nil
      -- end
    -- end
  -- end
-- end

Citizen.CreateThreadNow(function()
  -- generate menus
  for k,v in pairs(cfg.weapons_kits_types) do
    local menu = {name=v._config.title,css={top="75px",header_color="rgba(0,125,255,0.75)"}}
    menus[k] = menu
	
    -- choose kit 
    local choose = function(player, choice)
      local kit = v[choice]
      if kit then
        vRPclient.giveWeapons(player, kit, true)
        vRPclient.setHealth(player,1000)
	  end
    end

    -- add cloak choices
    for l,w in pairs(v) do
      if l ~= "_config" then
        menu[l] = {choose}
      end
    end
  end
end)

-- clients points

local function build_client_points(source)
  for k,v in pairs(cfg.weapons_kits) do
    local gtype,x,y,z = table.unpack(v)
    local weapons_kits = cfg.weapons_kits_types[gtype]
    local menu = menus[gtype]
    if weapons_kits and menu then
      local gcfg = weapons_kits._config or {}

      local function weapons_kits_enter(source,area)
        local user_id = vRP.getUserId(source)
        if user_id and vRP.hasPermissions(user_id,gcfg.permissions or {}) then
          vRP.openMenu(source,menu)
        end
      end

      local function weapons_kits_leave(source,area)
        vRP.closeMenu(source)
      end

      vRPclient.addMarker(source,x,y,z-1,0.7,0.7,0.5,0,125,255,125,150)
      vRP.setArea(source,"vRP:cfg:weapons_kits"..k,x,y,z,1,1.5,weapons_kits_enter,weapons_kits_leave)
    end
  end
end

-- add points on first spawn
AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  if first_spawn then
    build_client_points(source)
  end
end)
