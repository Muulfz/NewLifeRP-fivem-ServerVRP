
local cfg = {}

-- paycheck and bill for users
cfg.message_paycheck = "Você recebeu seu salario:  ~g~R$" -- message that will show before payment of salary
cfg.message_bill = "Pagamento de contas:  ~r~R$" -- message that will show before payment of bill
cfg.post = "." -- message that will show after payment

cfg.bank = true -- if true money goes to bank, false goes to wallet

cfg.minutes_paycheck = 5 -- minutes between payment for paycheck
cfg.minutes_bill = 60 -- minutes between withdrawal for bill

cfg.paycheck_title_picture = "Caixa Economica" -- define title for paycheck notification picture
cfg.paycheck_picture = "CHAR_BANK_MAZE" -- define paycheck notification picture want's to display
cfg.bill_title_picture = "Compania de Seguro" -- define title for bill notification picture
cfg.bill_picture = "CHAR_MP_MORS_MUTUAL" -- define bill notification picture want's to display

cfg.paycheck = { -- ["permission"] = paycheck
  ["pm_recruta.paycheck"] = 41,
  ["pm_soldado.paycheck"] = 94,
  ["pm_cabo.paycheck"] = 123,
  ["pm_3sargento.paycheck"] = 135,
  ["pm_2sargento.paycheck"] = 143,
  ["pm_1sargento.paycheck"] = 156,
  ["pm_subtenente.paycheck"] = 212,
  ["pm_2tenente.paycheck"] = 223,
  ["pm_1tenente.paycheck"] = 242,
  ["pm_capitao.paycheck"] = 258,
  ["pm_major.paycheck"] = 272,
--[""] = 0,
  ["aguia_recruta.paycheck"] = 47,
  ["aguia_soldado.paycheck"] = 108,
  ["aguia_cabo.paycheck"] = 141,
  ["aguia_3sargento.paycheck"] = 155,
  ["aguia_2sargento.paycheck"] = 164,
  ["aguia_1sargento.paycheck"] = 179,
  ["aguia_subtenente.paycheck"] = 243,
  ["aguia_2tenente.paycheck"] = 256,
  ["aguia_1tenente.paycheck"] = 277,
  ["aguia_capitao.paycheck"] = 296,
  ["aguia_major.paycheck"] = 312,
--[""] = 0,
  ["rocam_recruta.paycheck"] = 43,
  ["rocam_soldado.paycheck"] = 99,
  ["rocam_cabo.paycheck"] = 130,
  ["rocam_3sargento.paycheck"] = 143,
  ["rocam_2sargento.paycheck"] = 151,
  ["rocam_1sargento.paycheck"] = 166,
  ["rocam_subtenente.paycheck"] = 224,
  ["rocam_2tenente.paycheck"] = 236,
  ["rocam_1tenente.paycheck"] = 256,
  ["rocam_capitao.paycheck"] = 273,
  ["rocam_major.paycheck"] = 289,
--[""] = 0,
  ["taticarecruta.paycheck"] = 45,
  ["taticaSoldado.paycheck"] = 103,
  ["taticaCabo.paycheck"] = 134,
  ["tatica3sargento.paycheck"] = 148,
  ["tatica2sargento.paycheck"] = 157,
  ["tatica1sargento.paycheck"] = 172,
  ["taticaSubTenente.paycheck"] = 233,
  ["tatica2Tenente.paycheck"] = 245,
  ["tatica1Tenente.paycheck"] = 266,
  ["taticaCapitão.paycheck"] = 283,
  ["taticaMajor.paycheck"] = 298,
--[""] = 0,
  ["rota_recruta.paycheck"] = 47,
  ["rota_soldado.paycheck"] = 108,
  ["rota_cabo.paycheck"] = 140,
  ["rota_3sargento.paycheck"] = 155,
  ["rota_2sargento.paycheck"] = 164,
  ["rota_1sargento.paycheck"] = 179,
  ["rota_subtenente.paycheck"] = 243,
  ["rota_2tenente.paycheck"] = 256,
  ["rota_1tenente.paycheck"] = 277,
  ["rota_capitao.paycheck"] = 296,
  ["rota_major.paycheck"] = 311,
--[""] = 0,
  ["tenente_coronel.paycheck"] = 338,
  ["coronel.paycheck"] = 407,
--[""] = 0,
  ["delegado.paycheck"] = 271,
  ["perito.paycheck"] = 230,
  ["investigador.paycheck"] = 142,
  ["escrivao.paycheck"] = 142,
  ["inspetor.paycheck"] = 122,
  ["agente_especial.paycheck"] = 97,
  ["agente.paycheck"] = 65,
--[""] = 0,
  ["bombeiro_recruta.paycheck"] = 49,
  ["bombeiro_soldado.paycheck"] = 109,
  ["bombeiro_cabo.paycheck"] = 144,
  ["bombeiro_3sargento.paycheck"] = 159,
  ["bombeiro_2sargento.paycheck"] = 167,
  ["bombeiro_1sargento.paycheck"] = 183,
  ["bombeiro_subtenente.paycheck"] = 248,
  ["bombeiro_2tenente.paycheck"] = 261,
  ["bombeiro_1tenente.paycheck"] = 284,
  ["bombeiro_capitao.paycheck"] = 303,
  ["bombeiro_major.paycheck"] = 319,
--[""] = 0,
  ["soldadoprf.paycheck"] = 60,
  ["agenteprf.paycheck"] = 130,
  ["agenteoperacionalprf.paycheck"] = 160,
  ["inspetorprf.paycheck"] = 180,
  ["agenteespecialprf.paycheck"] = 200,
  ["sargentoprf.paycheck"] = 240,
  ["aspiranteprf.paycheck"] = 290,
  ["capitaoprf.paycheck"] = 310,
  ["majorprf.paycheck"] = 360,
  ["delegadoprf.paycheck"] = 450,
--[""] = 0,
  ["emergency.paycheck"] = 50,
  ["taxi.paycheck"] = 50,
  ["repair.paycheck"] = 50,
  ["bankdriver.paycheck"] = 50,
  ["diretorchefe.paycheck"] = 210,
  ["delivery.paycheck"] = 50,
  ["cafetão.paycheck"] = 210,
  ["aviario.paycheck"] = 210,
  ["produtor.paycheck"] = 210,
  ["achogue.paycheck"] = 210
}

cfg.bill = { -- ["permission"] = withdrawal
  ["pm_recruta.paycheck"] = 23,
  ["pm_soldado.paycheck"] = 51,
  ["pm_cabo.paycheck"] = 66,
  ["pm_3sargento.paycheck"] = 73,
  ["pm_2sargento.paycheck"] = 77,
  ["pm_1sargento.paycheck"] = 84,
  ["pm_subtenente.paycheck"] = 114,
  ["pm_2tenente.paycheck"] = 120,
  ["pm_1tenente.paycheck"] = 130,
  ["pm_capitao.paycheck"] = 139,
  ["pm_major.paycheck"] = 146,
--[""] = 0,
  ["aguia_recruta.paycheck"] = 26,
  ["aguia_soldado.paycheck"] = 59,
  ["aguia_cabo.paycheck"] = 76,
  ["aguia_3sargento.paycheck"] = 84,
  ["aguia_2sargento.paycheck"] = 88,
  ["aguia_1sargento.paycheck"] = 97,
  ["aguia_subtenente.paycheck"] = 131,
  ["aguia_2tenente.paycheck"] = 138,
  ["aguia_1tenente.paycheck"] = 150,
  ["aguia_capitao.paycheck"] = 160,
  ["aguia_major.paycheck"] = 168,
--[""] = 0,
  ["rocam_recruta.paycheck"] = 24,
  ["rocam_soldado.paycheck"] = 54,
  ["rocam_cabo.paycheck"] = 70,
  ["rocam_3sargento.paycheck"] = 77,
  ["rocam_2sargento.paycheck"] = 82,
  ["rocam_1sargento.paycheck"] = 89,
  ["rocam_subtenente.paycheck"] = 120,
  ["rocam_2tenente.paycheck"] = 127,
  ["rocam_1tenente.paycheck"] = 138,
  ["rocam_capitao.paycheck"] = 147,
  ["rocam_major.paycheck"] = 155,
--[""] = 0,
  ["tatica_recruta.paycheck"] = 25,
  ["tatica_soldado.paycheck"] = 56,
  ["tatica_cabo.paycheck"] = 73,
  ["tatica_3sargento.paycheck"] = 80,
  ["tatica_2sargento.paycheck"] = 84,
  ["tatica_1sargento.paycheck"] = 93,
  ["tatica_subtenente.paycheck"] = 125,
  ["tatica_2tenente.paycheck"] = 132,
  ["tatica_1tenente.paycheck"] = 143,
  ["tatica_capitao.paycheck"] = 153,
  ["tatica_major.paycheck"] = 160,
--[""] = 0, 
  ["rota_recruta.paycheck"] = 26,
  ["rota_soldado.paycheck"] = 58,
  ["rota_cabo.paycheck"] = 76,
  ["rota_3sargento.paycheck"] = 84,
  ["rota_2sargento.paycheck"] = 88,
  ["rota_1sargento.paycheck"] = 97,
  ["rota_subtenente.paycheck"] = 130,
  ["rota_2tenente.paycheck"] = 138,
  ["rota_1tenente.paycheck"] = 150,
  ["rota_capitao.paycheck"] = 160,
  ["rota_major.paycheck"] = 168,
--[""] = 0,
  ["tenente_coronel.paycheck"] = 183,
  ["coronel.paycheck"] = 220,
--[""] = 0,  
  ["soldadoprf.paycheck"] = 21,
  ["agenteprf.paycheck"] = 46,
  ["agenteoperacionalprf.paycheck"] = 56,
  ["inspetorprf.paycheck"] = 63,
  ["agenteespecialprf.paycheck"] = 70,
  ["sargentoprf.paycheck"] = 84,
  ["aspiranteprf.paycheck"] = 102,
  ["capitaoprf.paycheck"] = 109,
  ["majorprf.paycheck"] = 126,
  ["delegadoprf.paycheck"] = 158,
--[""] = 0,
  ["emergency.paycheck"] = 10,
  ["taxi.paycheck"] = 10,
  ["repair.paycheck"] = 10,
  ["bankdriver.paycheck"] = 10,
  ["diretorchefe.paycheck"] = 50,
  ["delivery.paycheck"] = 50
}

return cfg
